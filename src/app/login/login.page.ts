import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UtilService } from '../util.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  isLogin: boolean =false;

  constructor(
    private util: UtilService,
    private navCtrl: NavController, 
  ) { }

  ngOnInit() {
  }

  login() {
    // Enabling Side Menu
    this.util.setMenuState(true);
    this.navCtrl.navigateRoot('/home', { animationDirection: 'forward' });
    if(this._auth.getUserDetails() != null){

      this.isLogin = true;
  }
  }



}





